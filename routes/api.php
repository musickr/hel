<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::namespace('Admin\Api')->prefix('admin')->group(function (){
    Route::get('/index', 'IndexController@index');
    //用户列表
    Route::get('/users/list', 'UsersController@list');
    //用户充值
    Route::post('/user/recharge', 'UsersController@recharge');
    //资产列表
    Route::get('/propertys/list', 'PropertyController@list');
    //用户转账
    Route::post('/user/transfer', 'UsersController@transfer');
    //团队
    Route::get('/user/team','UsersController@team');
    //系统金额配置
    Route::get('/system/money','ConfigController@money');
    //修改升级费用
    Route::post('/system/money/edit','ConfigController@editMoney');
    //审核升级费用
    Route::get('/system/audit','ConfigController@audit');
    //编辑用户
    Route::post('/user/edit','UsersController@edit');
    //删除用户
    Route::post('/user/del','UsersController@del');
    //用户搜索
    Route::post('/users/search','UsersController@search');
});


