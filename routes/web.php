<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return '<h1 style="display: flex;justify-content: center">系统运行中！</h1>';
});
Route::namespace('Web\View')->prefix('app')->group(function (){
    //登陆页
    Route::get('/index','LoginController@index');

    Route::group(['middleware'=>'auth.app'],function (){
        //首页
        Route::get('/index/index','IndexController@index');
        //搜索
        Route::get('/team/search/{user_phone?}','TeamController@search');

        //团队
        Route::get('/team/index','TeamController@index');

        //我的
        Route::get('/me/index','MeController@index');
        //我要升级
        Route::get('/index/update','UpdateController@index');
        //我要升级2
        Route::get('/index/updateSub','UpdateSubController@index');
        //金币明细
        Route::get('/index/goldCoinDetail','GoldCoinDetailController@index');
        //金币转账
        Route::get('/index/goldCoinTransfer','GoldCoinTransferController@index');
        //修改密码
        Route::get('/me/editPwd','EditPwdController@index');
        //修改收款信息
        Route::get('/me/editPay','EditPayController@index');
        //注册页面
        Route::get('/team/register/{id?}','RegisterController@index');
        //注册收款信息
        Route::get('/me/registerPay','RegisterPayController@index');
        //我要审核
        Route::get('/index/audit','AuditController@index');
        //公用加载页面
        Route::get('/loading','LoadingController@index');
        //公告列表
        Route::get('/notify/list','NotifyController@list');
        //公告页
        Route::get('/notify/{id?}','NotifyController@index');
        //校验升级资格
        Route::post('/checkRole','UpdateController@check');
        //确认升级
        Route::post('/upTrue','UpdateSubController@order');
        //审核列表
        Route::post('/audit/list','AuditController@type');
        //审核操作
        Route::post('/audit/opr','AuditController@opr');
        //注册用户
        Route::post('/user/register','RegisterController@add');
        //推出
        Route::get('/me/out','MeController@loginOut');
        //注册账户信息
        Route::post('/pay/register','RegisterPayController@add');
        //修改账户信息
        Route::post('/pay/edit','editPayController@edit');
        //金币转账
        Route::post('/Coin/transfer','GoldCoinTransferController@transfer');
        //修改密码
        Route::post('/me/edit','EditPwdController@edit');
        //在线留言
        Route::get('/me/msg','ServerController@index');
        //发送留言
        Route::post('/msg/send','ServerController@add');

    });
    //登陆
    Route::middleware('guest.app')->post('/login','LoginController@login');
    //忘记密码
    Route::get('user/forget/pwd','LoginController@forge');
});

Auth::routes();


Route::get('/admin', 'HomeController@index')->name('home');

Route::namespace('Admin\View')->prefix('admin')->group(function (){
    Route::get('/index', 'IndexController@index');
    Route::get('/users/list', 'UsersController@index');
    //注册用户
    Route::get('/users/register', 'UsersController@register');
    //资产列表
    Route::get('/propertys/list', 'PropertyController@index');
    //团队关系
    Route::get('/users/team', 'UsersController@team');
    //系统配置（金额配置）
    Route::get('/system/money', 'ConfigController@index');
    //审核配置
    Route::get('/system/audit','ConfigController@audit');

});

Route::get('/code','Web\View\CodeController@send');

