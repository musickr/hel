@extends('public.app.head')

@section('content')
    <div class="goldCoinDetail">
        <header>
            <span onclick="window.history.back(-1)"></span>金币明细
        </header>
        <div style="height: 1.1rem"></div>
        @foreach($details as $detail)
        <div class="goldCoinDetail-item">
            <div class="text">
                <p>{{ $detail->property_remark }}</p>
                <span>2018/11/10 18：00</span>
            </div>
            <div class="num" >
                <span @if($detail -> property_num < 0) style="color:#fe0000" @endif>{{ $detail -> property_num }}</span>
            </div>
        </div>
        @endforeach
        <div style="margin-bottom: 3.3rem"></div>
    </div>

@endsection
@section('footer')
    @endsection
@section('js')
    <script>
        $('.goldCoinDetail').css('height',window.innerHeight+'px');
    </script>
@endsection
