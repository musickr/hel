@extends('public.app.head')

@section('content')
    <div class="goldCoinDetail" style="background: #ffffff">
        <header>
            <span onclick="window.history.back(-1)"></span>{{ $notify->notify_title }}
        </header>
        <div style="height: 1.1rem"></div>
        <div style="text-align: center;font-size: 0.4rem;padding: 0.5rem 0 0.2rem 0;margin:0 0.3rem;border-bottom: 0.02rem solid #f8f8f8">
            <h2 style="font-weight: 600;">{{ $notify->notify_title }}</h2>
            <small style="display: block;margin-right:0.2rem;text-align: right;font-size: 0.25rem;padding-top:0.3rem;color: #999999; ">{{ $notify->created_at }}</small>
        </div>
        <div style="padding:0.3rem 0.5rem;text-indent:0.6rem;text-align:justify!important;line-height: 1.5">{{ $notify->notify_content }}</div>
        <div style="margin-bottom: 3.3rem"></div>
    </div>

@endsection
@section('footer')
    @endsection
@section('js')
    <script>
        $('.goldCoinDetail').css('height',window.innerHeight+'px');
    </script>
@endsection
