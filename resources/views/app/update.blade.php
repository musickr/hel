@extends('public.app.head')
@section('style')
    <style>
        a,a:hover,a:active,a:visited,a:link,a:focus{

            outline:none;
            color: #ffffff;
            text-decoration: none;
        }
        .btm {
            outline: none;
            text-decoration: none;
            padding: 0;
            line-height: 1rem;

            height: 1rem;
            width: 5.95rem;
            margin: 0.45rem auto;
            background: #183159;
            color: #ffffff;
            border: none;
            font-size: 0.3rem;
            display: block;
            border-radius: 0.15rem;
            text-align: center;
        }
    </style>
    @endsection
@section('content')
    <div class="update">
        <header style="background: none;color:#ffffff;box-shadow: none">
            <span onclick="window.history.back(-1)" style="border-color: #ffffff"></span>我要升级
        </header>
        <div class="update-banner" style="background: url('{{ asset('image/update1.png') }}') no-repeat;background-size: 100%;">
            <p style="color: #ffffff">当前等级：{{ $userRole }}</p>
        </div>
        <div class="update-list">
            <ul>
                @foreach($parents as $parent)
                <li>
                    <p style="width: 50%">上{{ $parent->user_node }}层</p>
                    <p style="width: 50%">{{ $parent->user_account }}</p>
                </li>
                @endforeach
            </ul>
            <a href="javascript:;" class="btm" >我要升级</a>
        </div>
        <div style="margin-bottom: 3.3rem"></div>
    </div>

@endsection
@section('footer')
@endsection
@section('js')
    <script>
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $('.update').css('height',window.innerHeight+'px');
        $('a').on('click',function(){
            $.post("{{ url('app/checkRole') }}",function (data) {
                if(data.res){
                    $.get('{{ url("app/index/updateSub") }}',function (data) {
                        if(data.status=='error')
                        {
                            layer.open({
                                content: data.msg
                                ,skin: 'msg'
                                ,time: 1 //2秒后自动关闭
                            });
                        }
                        else {window.location.href = "{{ url('app/index/updateSub') }}";}
                    });

                }
            },'JSON')
        })
    </script>
@endsection
