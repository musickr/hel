@extends('public.app.head')
@section('style')
    <style>
        body {
            margin:0 auto;
            overflow:hidden;
        }
        .login
        {
            width: 100%;
            position: relative;
        }
        canvas {
            width:100%;height:auto/*默认全屏显示 可自己设置高度640px*/;
            display:inline-block;
            vertical-align:baseline;
            position:absolute;
            z-index:-1;
        }
        .login{
            position:relative ;
        }
        .login img{
            position: fixed;

            width: 100%;
            z-index: -1;
        }
        .logo{
            width: 100%;
            height: auto;
            display: flex;
            justify-content: center;
            padding: 1.5rem 0 0.6rem 0;
            text-align: center;
        }
        .logo img{
            width: 3rem;
        }
        .app-form{
            width: 100%;
            margin-top: 2.5rem;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .app-form-item{
            width: 80%;
        }
        .login-input{
            width: 100%;
            height: 0.8rem;
            outline: none;
            background: none;
            text-align: center;
            border:0.01rem  solid #666666;
            color: #FFFFFF;
            font-size: 0.3rem;
        }
        .login-btn{
            width: 100%;
            height: 0.8rem;
            outline: none;
            background: #3a4b55;
            text-align: center;
            border:0.01rem  solid #666666;
            color: #FFFFFF;
            font-size: 0.3rem;
            margin-top: 0.85rem;
        }
        .login a{
            width: 100%;
            text-align: center;
            font-size: 0.25rem;
            display: block;
            color: #ffff;
            margin-top: 0.4rem;
        }

    </style>
@endsection
@section('content')
    <div class="login">
        <canvas id="canvas"></canvas>
        <div class="logo">
            <img src="{{ asset('image/logo.png') }}">
        </div>
        <form class="app-form" action="{{ url('app/login') }}" method="post">
            @csrf
            <div class="app-form-item">
                <input class="login-input" name="user_account" value="" type="text" placeholder="请输入你的账号">
            </div>
            <div class="app-form-item">
                <input class="login-input" name="user_pwd" value="" type="password" placeholder="请输入你的密码">
            </div>
            <div class="app-form-item">
                <button class="login-btn">确定</button>
            </div>
        </form>
        <a style="text-align: right;width: 90%" href="{{ url('app/user/forget/pwd') }}">忘记密码 ?</a>
    </div>

@endsection
@section('footer')
    @endsection
@section('js')
    <script>
        $('#canvas').css({'height':window.innerHeight+'px'})
        //宇宙特效

        var canvas = document.getElementById('canvas'),
            ctx = canvas.getContext('2d'),
            w = canvas.width = window.innerWidth,
            h = canvas.height = window.innerHeight,

            hue = 217,
            stars = [],
            count = 0,
            maxStars = 180;//星星数量

        var canvas2 = document.createElement('canvas'),
            ctx2 = canvas2.getContext('2d');
        canvas2.width = 100;
        canvas2.height = 100;
        var half = canvas2.width / 2,
            gradient2 = ctx2.createRadialGradient(half, half, 0, half, half, half);
        gradient2.addColorStop(0.025, '#CCC');
        gradient2.addColorStop(0.1, 'hsl(' + hue + ', 61%, 33%)');
        gradient2.addColorStop(0.25, 'hsl(' + hue + ', 64%, 6%)');
        gradient2.addColorStop(1, 'transparent');

        ctx2.fillStyle = gradient2;
        ctx2.beginPath();
        ctx2.arc(half, half, half, 0, Math.PI * 2);
        ctx2.fill();

        // End cache

        function random(min, max) {
            if (arguments.length < 2) {
                max = min;
                min = 0;
            }

            if (min > max) {
                var hold = max;
                max = min;
                min = hold;
            }

            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function maxOrbit(x, y) {
            var max = Math.max(x, y),
                diameter = Math.round(Math.sqrt(max * max + max * max));
            return diameter / 3;
            //星星移动范围，值越大范围越小，
        }

        var Star = function() {

            this.orbitRadius = random(maxOrbit(w, h));
            this.radius = random(60, this.orbitRadius) / 8;
            //星星大小
            this.orbitX = w / 2;
            this.orbitY = h / 2;
            this.timePassed = random(0, maxStars);
            this.speed = random(this.orbitRadius) / 50000;
            //星星移动速度
            this.alpha = random(2, 10) / 10;

            count++;
            stars[count] = this;
        };

        Star.prototype.draw = function() {
            var x = Math.sin(this.timePassed) * this.orbitRadius + this.orbitX,
                y = Math.cos(this.timePassed) * this.orbitRadius + this.orbitY,
                twinkle = random(10);

            if (twinkle === 1 && this.alpha > 0) {
                this.alpha -= 0.05;
            } else if (twinkle === 2 && this.alpha < 1) {
                this.alpha += 0.05;
            }

            ctx.globalAlpha = this.alpha;
            ctx.drawImage(canvas2, x - this.radius / 2, y - this.radius / 2, this.radius, this.radius);
            this.timePassed += this.speed;
        }

        for (var i = 0; i < maxStars; i++) {
            new Star();
        }

        function animation() {
            ctx.globalCompositeOperation = 'source-over';
            ctx.globalAlpha = 0.5; //尾巴
            ctx.fillStyle = 'hsla(' + hue + ', 64%, 6%, 2)';
            ctx.fillRect(0, 0, w, h)

            ctx.globalCompositeOperation = 'lighter';
            for (var i = 1, l = stars.length; i < l; i++) {
                stars[i].draw();
            };

            window.requestAnimationFrame(animation);
        }

        animation();
    </script>
@endsection

