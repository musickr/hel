@extends('public.app.head')
@section('style')
    <style>
        a,a:hover,a:active,a:visited,a:link,a:focus{

            outline:none;
            color: #ffffff;
            text-decoration: none;
        }
        .btm {
            outline: none;
            text-decoration: none;
            padding: 0;
            line-height: 1rem;
            border: none;
            height: 1rem;
            width: 5.95rem;
            margin: 0.45rem auto;
            background: #183159;
            color: #ffffff !important;
            font-size: 0.3rem;
            display: block;
            border-radius: 0.15rem;
            text-align: center;
        }
        .lay:active{
            display: none;
        }
        .chose{
            width: 0.45rem;
            height: 0.45rem;
            margin-right:0.1rem ;
        }
    </style>

    @endsection
@section('content')
    <div class="updateSub">
        <div class="lay"  style="display: none" >
            <form class="lay-bg" style="background: url({{ asset('image/pay-type.png') }}) no-repeat;">
                <p style="">请选择支付方式</p>
                <ul style="">
                    <li style="display: flex;align-items: center" onclick="ed($(this))">
                        <img class="chose" id="1" src="{{ asset('image/chose.png') }}">
                        <label>支付宝</label>
                    </li>
                    <li style="display: flex;align-items: center" onclick="ed($(this))">
                        <img class="chose" id="3" src="{{ asset('image/chose.png') }}">
                        <label>银行卡</label>
                    </li>
                    <li style="display: flex;align-items: center" onclick="ed($(this))">
                        <img class="chose" id="2" src="{{ asset('image/chose.png') }}">
                        <label>微信</label>
                    </li>
                </ul>
                <button type="button" onclick="to()">确定</button>
            </form>
        </div>

        <header style="background: none;color:#ffffff;box-shadow: none">
            <span onclick="window.history.back(-1); " style="border-color: #ffffff"></span>我要升级
        </header>
        <div class="update-banner" style="background: url('{{ asset('image/update2.png') }}') no-repeat;background-size: 100%;">
            <div style="color: #ffffff" class="money">
                <p>{{ $money }}</p>
                <span>收款金额</span>
            </div>
        </div>

        @empty(!$account['account'])
        <div class="pay-msg">
            <form style="width: 85%;margin-top: 0.5rem">
                @foreach($account['account'] as $value)

                    <div class="form-item">
                        <label for="name">开户姓名:</label>
                        <input type="text" id="name" name="" readonly value="{{ $value['ac_name'] }}" style="width: 77%"/>
                    </div>
                    <div class="form-item">
                        <label for="bank-name">开户银行:</label>
                        <input type="text" id="bank-name" name="" readonly  value="{{ $value['ac_bank_name'] }}" style="width: 77%"/>
                    </div>
                    <div class="form-item">
                        <label for="bank-num">银行卡号:</label>
                        <input type="text" id="bank-num" name="" readonly value="{{ $value['ac_bank_num'] }}"/>
                        <a  class="btm" style="width: 15%;height: 0.55rem;line-height: 0.55rem;margin: 0;border-radius: 0.1rem" >复制</a>
                    </div>

                    <div class="form-item">
                        <label for="wei">微信账号:</label>
                        <input type="text" id="wei" name="" readonly value="{{ $value['ac_wechat'] }}"/>
                        <a  class="btm" style="width: 15%;height: 0.55rem;line-height: 0.55rem;margin: 0;border-radius: 0.1rem" >复制</a>
                    </div>

                    <div class="form-item">
                        <label for="zpay">支付宝:</label>
                        <input type="text" id="zpay" readonly name="" value="{{ $value['ac_zpay'] }}"/>
                        <a class="btm" style="width: 15%;height: 0.55rem;line-height: 0.55rem;margin: 0;border-radius: 0.1rem" id="copy">复制</a>
                    </div>

                @endforeach
            </form>
            <a class="btm" onclick="lay()">我已转账确定升级</a>
        </div>
        @endempty
        <div style="margin-bottom: 3.3rem"></div>
    </div>

@endsection
@section('footer')
@endsection
@section('js')
    <script>
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        var type = null;
        $('.updateSub').css('height',window.innerHeight+'px');
        function lay()
        {
        let lay = $('.lay');
        lay.css('width',window.innerWidth + 'px');
        lay.css('height',window.innerHeight + 'px');
        lay.css('background','rgba(0,0,0,0.4)');
        lay.css('position','fixed');
        lay.css('top','0');
        lay.css('zIndex','1000');
        lay.css('display','flex');
        lay.css('justify-content','center');
        lay.css('align-items','center');
        }
        $('.lay').on('click',function () {
          $(this).css('display','none');
        });
        function ed(obj) {
            obj.find('img').attr('src',"{{ asset('image/chosed.png') }}");
            type = obj.find('img').attr('id');
            obj.siblings().find('img').attr('src',"{{ asset('image/chose.png') }}");
            event.stopPropagation();
        }
        function to() {
            let data = {
                order_in:"{{ $sub['order_in'] }}",
                order_to:"{{ $sub['order_to'] }}",
                order_type:type,
                money:"{{ $money }}"
            };
            $.post("{{ url('app/upTrue') }}",data,function (data) {
                console.log(data)
                if(data)
                {
                    window.history.back(-1);
                }
            })
            console.log(data)
        }
        $('a').click(function () {
            $(this).prev().select();
            document.execCommand("Copy");

        });




    </script>
@endsection
