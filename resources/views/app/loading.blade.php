<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta http-equiv="Refresh" content="1;url={{ $url }}" />
    <title>加载中</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        #loading {
/*
            background: rgba(0, 0, 0, .2);
*/
            height: 100%;
            width: 100%;
            position: fixed;
            z-index: 1;
            margin-top: 0;
            top: 0;
        }

        #loading-center {
            width: 100%;
            height: 100%;
            position: relative;
            display: flex;
            justify-content: center;
            align-items: center;

        }

        #loading-center-absolute {
            position: absolute;
            width: 50px;
            height: 50px;
        }

        .object {
            width: 10%;
            height: 10%;
            background-color: #FFF;
            position: absolute;
            -moz-border-radius: 100%;
            -webkit-border-radius: 100%;
            border-radius: 100%;
            -webkit-animation: animate 0.8s infinite;
            animation: animate 0.8s infinite;
        }

        #object_one {
            top: 12.66666%;
            left: 12.66666%;

        }

        #object_two {
            top: 0;
            left: 43.33333%;
            -webkit-animation-delay: 0.1s;
            animation-delay: 0.1s;

        }

        #object_three {
            top: 12.66666%;
            left: 74%;
            -webkit-animation-delay: 0.2s;
            animation-delay: 0.2s;

        }

        #object_four {
            top: 43.33333%;
            left: 86.6666%;
            -webkit-animation-delay: 0.3s;
            animation-delay: 0.3s;
        }

        #object_five {
            top: 74%;
            left: 74%;
            -webkit-animation-delay: 0.4s;
            animation-delay: 0.4s;
        }

        #object_six {
            top: 86.6666%;
            left: 43.33333%;
            -webkit-animation-delay: 0.5s;
            animation-delay: 0.5s;
        }

        #object_seven {
            top: 74%;
            left: 12.66666%;
            -webkit-animation-delay: 0.6s;
            animation-delay: 0.6s;
        }

        #object_eight {
            top: 43.33333%;
            left: 0;
            -webkit-animation-delay: 0.7s;
            animation-delay: 0.7s;
        }

        @-webkit-keyframes animate {

            25% {
                -ms-transform: scale(1.3);
                -webkit-transform: scale(1.3);
                transform: scale(1.3);
            }
            75% {
                -ms-transform: scale(0);
                -webkit-transform: scale(0);
                transform: scale(0);
            }
        }

        @keyframes animate {
            50% {
                -ms-transform: scale(1.3, 1.3);
                -webkit-transform: scale(1.3, 1.3);
                transform: scale(1.3, 1.3);
            }

            100% {
                -ms-transform: scale(1, 1);
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1);
            }

        }
    </style>
</head>
<body style="background: #dddddd">
<div id="loading">
    <div id="loading-center">
        <div id="loading-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
            <div class="object" id="object_four"></div>
            <div class="object" id="object_five"></div>
            <div class="object" id="object_six"></div>
            <div class="object" id="object_seven"></div>
            <div class="object" id="object_eight"></div>
        </div>
        <p style="position: absolute;top: 55%;color: #ffffff"> {{ $msg }} </p>
    </div>

</div>
<script>


</script>
</body>
</html>
