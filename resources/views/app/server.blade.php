@extends('public.app.head')
@section('style')
    <style>
        a,a:hover,a:active,a:visited,a:link,a:focus{

            outline:none;
            color: #ffffff;
            text-decoration: none;
        }
        .btm {
            outline: none;
            text-decoration: none;
            padding: 0;
            line-height: 1rem;

            height: 1rem;
            width: 5.95rem;
            margin: 0.45rem auto;
            background: #183159;
            color: #ffffff;
            border: none;
            font-size: 0.3rem;
            display: block;
            border-radius: 0.15rem;
            text-align: center;
        }
        .msgBox{
            width: 100%;
            position: fixed;
            bottom: 0;
            z-index: 100;
            height: 1.1rem;
            display: flex;
            align-items: center;
            background: #ffffff;
            box-shadow: 0 0 0.1rem #d6d6d6;
            justify-content: space-around;
            padding: 0 0.5rem;
            box-sizing: border-box;
        }
        .msgBox .msgInp{
            flex: auto;
            height: 0.6rem;
            background: none;
            border-top: none;
            border-left: none;
            border-right: none;
            font-size: 0.3rem;
        }
        .msgBox .msgInp:focus {
            outline:none;
            border-bottom: 1px solid #f6993f;
        }
        .msgBox .subBtn{
            outline:none;
            flex: auto;
            margin-left: 0.3rem;
            height: 0.6rem;
            background: none;
            border: none;
            box-shadow: 0 0 0.06rem #d6d6d6;
            color: #3490dc;
            border-radius: 0.3rem;
        }
        ul li {
            width: 100%;
            padding:0.2rem;
            box-sizing: border-box;
        }
        .self{
            display: flex;
            justify-content: safe;
            flex-direction: row;
        }
        .self img{
            width: 0.8rem;
            height: 0.8rem;
            padding: 0 0.1rem;
        }
        .self p{
            width: 60%;
            background: #ffffff;
            border-radius: 0.2rem;
            height: auto;
            padding: 0.2rem;
            display: flex;
            align-items: center;
            box-shadow: 0 0 0.1rem #d6d6d6;
        }
    </style>
@endsection
@section('content')
    <div class="goldCoinDetail">
        <header>
            <span onclick="window.history.back(-1)"></span>在线留言
        </header>
        <div style="height: 1.1rem"></div>
        <ul>
            @foreach($user->massages as $massage)
            <li class="self" @if($massage->msg_type==-1) style="flex-direction: row-reverse"  @endif">
                <img @if($massage->msg_type==1)src="{{  asset('image/self.png')  }}@else src="{{  asset('image/server.png')  }} @endif">
                <p>{{ $massage->msg_content }}</p>
            </li>
            @endforeach
        </ul>
        <div style="margin-bottom: 3.3rem"></div>
    </div>
@endsection
@section('footer')
    <div class="msgBox">
        <input type="text" class="msgInp" id="input" name="msg_content" value="" />
        <button class="subBtn" onclick="send({{ $user->user_id }})">发送</button>
    </div>
@endsection
@section('js')
    <script>
        document.querySelector('#input').scrollIntoView();

        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $('.goldCoinDetail').css('height',window.innerHeight+'px');
        function send(uid) {
            let content = $('input[name=msg_content]').val();
            if(content){
                $.post("{{ url('app/msg/send') }}",{
                    msg_uid:uid,
                    msg_content:content,
                    msg_type:1
                },function () {
                    let html =
                        "<li class='self' >" +
                        "<img src='{{ asset("image/self.png") }}'>" +
                        "<p>"+content+"</p>" +
                        "</li>";
                    $('ul').append(html);
                })
            }
        }
    </script>
@endsection
