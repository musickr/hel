@extends('public.app.head')

@section('content')
    <div class="audit">
        <header>
            <span onclick="window.history.back(-1)"></span>我要审核
        </header>
        <div style="height: 1.1rem"></div>
        <div class="tab">
            <p class="tab-item" s="2" style="color:#16325a;border-bottom: 0.03rem solid #16325a ">
                <img src="{{ asset('image/shi-1.png') }}"><span>待审核</span>
            </p>
            <i style="width: 0.03rem;height: 0.4rem;background: #ededed;display: inline-block;margin: 0 0.8rem"></i>
            <p class="tab-item" s="1">
                <img src="{{ asset('image/she.png') }}"><span>已通过</span>
            </p>
        </div>
        <div class="nav" style="" id="0">


        </div>
        <div class="nav" style="display: none"></div>
        <div class="nav" style="display: none" id="2">
            <div class="audit-list">
                <img src="{{ asset('image/she-1.png') }}">
                <div class="audit-userInfo">
                    <p>【张三三】  18081237157</p>
                    <p>支付宝付款</p>
                    <small style="color: #7b7b7b">2018/11/09 08:22:00</small>
                </div>
                <div class="audit-btn">
                    <p style="background: #ff7e00;margin-top: 0">通过</p>
                    <p style="background: #304562">不通过</p>
                </div>
            </div>
        </div>
        <div style="margin-bottom: 3.3rem"></div>
    </div>

@endsection
@section('js')
    <script>
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        int(2);


        $('.audit').css('height',window.innerHeight+'px');
        $('.tab-item').on('click',function () {
            let status = $(this).attr('s');
            int(status);
            $(this).css({borderBottom:'0.03rem solid #16325a',color:'#16325a'});
            if($(this).index() == 0)
            {
                $(this).find('img').attr('src','/image/shi-1.png');
                $(this).siblings().find('img').attr('src','/image/she.png');;
            }
            if($(this).index() == 2)
            {
                $(this).find('img').attr('src','/image/she-1.png');
                $(this).siblings().find('img').attr('src','/image/shi.png');;
            }

            $(this).siblings().css({borderBottom:'none',color:'#686868'});
            for (var i=0;i<=$('.nav').length;i++)
            {
                if(i == $(this).index())
                {
                    $('#'+i).css('display','flex')
                }
                else {
                    $('#'+i).css('display','none')
                }
            }

        })


        function int(status,callback)
        {

            $.post("{{ url('app/audit/list') }}",{status:status},function (data) {
                let html = '';

                if(data.length > 0){
                    for (var i = 0;i < data.length;i++){
                        let type = {
                            1:'支付宝',
                            2:'微信',
                            3:'银行转账'
                        };
                        html += "<div class=\"audit-list\" style=\"border-radius: 0.2rem\">\n" +
                            "                <img src=\"{{ asset('image/shi-1.png') }}\">\n" +
                            "                <div class=\"audit-userInfo\">\n" +
                            "                    <p>【"+data[i].app_user.user_account+"】"+data[i].app_user.user_phone+"</p>\n" +
                            "                    <p>"+type[data[i].order_type]+"付款</p>\n" +
                            "                    <small style=\"color: #7b7b7b\">"+data[i].created_at+"</small>\n" +
                            "                </div>\n" +
                            "                <div  class=\"audit-btn\">\n" ;
                        if(data[i].order_status==2){
                        html +=
                            "                    <p onclick=opr("+data[i].order_id+","+data[i].order_status+",1) style=\"background: #ff7e00;margin-top: 0\">通过</p>\n" +
                            "                    <p onclick=opr("+data[i].order_id+","+data[i].order_status+",-1) style=\"background: #304562\">不通过</p>\n"
                        }
                        html+=
                            "                </div>\n" +
                            "            </div>"

                    }

                }
                else{
                    html = '<p>暂无数据</p>'
                }
                $('.nav').html(html);


            });

        }
        function opr(oid,type,status) {
            let date = {
                order_id:oid,
                order_status:status
            }
            $.post("{{ url('app/audit/opr') }}",date,function (data) {
                console.log(data.msg)
            });
            int(type);
        }
    </script>
@endsection
