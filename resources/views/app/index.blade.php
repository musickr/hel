@extends('public.app.head')
@section('style')
    <style>
        a:active{
            -webkit-tap-highlight-color:rgba(0,0,0,0);
        }
    </style>
@endsection
@section('content')
    <div class="index">
       <div class="banner" id="large-header" style="background: #0d1134">
           <canvas id="demo-canvas" style="position: fixed;"></canvas>
           <div class="info">
               <div class="user">
                   <div class="user-info">
                       <img src="{{ $user->user_photo }}" class="user-photo" style="border-radius: 50%">
                       <p class="user-phone">
                           <?php
                           echo  preg_replace('/(1[35478]{1}[0-9])[0-9]{4}([0-9]{4})/i', '$1****$2', $user->user_phone)
                           ?>
                           </p>
                   </div>
                   <div class="user-role">
                       <img src="{{ asset('image/role.png') }}" class="role-icon">
                       <p class="role-text">VIP{{ $user->user_role }}</p>
                   </div>
               </div>
               <div class="set">
                   <img src="{{ asset('image/set.png') }}">
               </div>
           </div>
           <div class="num">
               <p class="num-title">金币数量</p>
               <p class="num-num">{{ sprintf("%.2f",$user->property()->sum('property_num')) }}</p>
           </div>
           <div class="panel">
               <a class="panel-item" href="{{ url('app/index/update') }}" style="border-left: 0">
                   <img src="{{ asset('image/up.png') }}">
                   <span>我要升级</span>
               </a>
               <a class="panel-item" href="{{ url('app/index/audit') }}">
                   <img src="{{ asset('image/sh.png') }}">
                   <span>我要审核</span>
               </a>
           </div>E
           <div class="note">
               <div style="display: flex;align-items: center;width: 100%">
                   <img src="{{ asset('image/note.png') }}">
                   <span style="width: 1.5rem;">通知：</span>
                   <a href="{{ url('app/notify/'.$notify->notify_id) }}" style="color: #000000;width: 100%;display: inline">
                       <marquee direction="left" align="middle" width="90%">{{ $notify->notify_title }}</marquee>
                   </a>
               </div>
           </div>
       </div>

       <div class="box">
           <a href="{{ url('app/notify/list') }}"><img src="{{ asset('image/new-note.png') }}"><p>最新公告</p></a>
           <a href="{{ url('/app/team/index') }}"><img src="{{ asset('image/my-team.png') }}"><p>我的团队</p></a>
           <a href="{{ url('/app/index/goldCoinTransfer') }}" ><img src="{{ asset('image/jbzz.png') }}"><p>金币转账</p></a>
           <a href="{{ url('/app/me/msg') }}"><img src="{{ asset('image/ly.png') }}"><p>在线留言</p></a>
           <a href="{{ url('/app/me/registerPay') }}"><img src="{{ asset('image/sk.png') }}"><p>收款信息</p></a>
           <a href="{{ url('/app/index/goldCoinDetail') }}"><img src="{{ asset('image/jbmx.png') }}"><p>金币明细</p></a>
       </div>

    </div>
    <div style="background: #ffffff;height: 1.6rem">

    </div>
@endsection
@section('footer')
    <footer>
        <div class="footer">
            <a href="{{ url('/app/index/index') }}" class="tab-index">
                <img <?php if(explode('/',url()->current())[4] == 'index'){?>src="{{ asset('image/home-1.png') }}"<?php }else{?> src="{{ asset('image/home-0.png') }}" <?php }?> class="tab-icon">
                <p class="tab-title">首页</p>
            </a>

            <a href="{{ url('/app/team/index') }}" class="tab-index">
                <img <?php if(explode('/',url()->current())[4] == 'team'){?>src="{{ asset('image/team-1.png') }}"<?php }else{?> src="{{ asset('image/team-0.png') }}" <?php }?> class="tab-icon">
                <p class="tab-title">团队</p>
            </a>

            <a href="{{ url('app/me/index') }}" class="tab-index">
                <img <?php if(explode('/',url()->current())[4] == 'me'){?>src="{{ asset('image/me-1.png') }}"<?php }else{?> src="{{ asset('image/me-0.png') }}" <?php }?> class="tab-icon">
                <p class="tab-title">我的</p>
            </a>
        </div>
    </footer>
@endsection
@section('js')
    <script src="{{ asset('js/TweenLite.min.js') }}"></script>
    <script src="{{ asset('js/EasePack.min.js') }}"></script>
    <script src="{{ asset('js/dong.js') }}"></script>
@endsection
