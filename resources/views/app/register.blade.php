@extends('public.app.head')
@section('style')
    <style>
        a,a:hover,a:active,a:visited,a:link,a:focus{

            outline:none;
            color: #ffffff;
            text-decoration: none;
        }
        .btm {
            outline: none;
            text-decoration: none;
            padding: 0;
            line-height: 1rem;

            height: 1rem;
            width: 5.95rem;
            margin: 0.45rem auto;
            background: #183159;
            color: #ffffff;
            border: none;
            font-size: 0.3rem;
            display: block;
            border-radius: 0.15rem;
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <div class="updateSub">
        <header>
            <span onclick="window.history.back(-1)"></span>注册
        </header>
        <div style="height: 1.1rem"></div>
        <div class="pay-msg">
            <form style="width: 85%;margin-top: 0.5rem" action="{{ url('app/user/register') }}" method="post">
                @csrf
            <div class="form-item">
                <label for="name">直推人:</label>
                <input type="text" id="name" disabled  value="{{ $referrer->user_account }}" style="width: 75%"/>
                <input type="hidden" name="user_referrer"  value="{{ $referrer->user_id }}" style="width: 75%"/>
            </div>
            <div class="form-item">
                <label for="node">接点人:</label>
                <input type="hidden"  name="user_pid" value="{{ $node->user_id }}" style="width: 75%"/>
                <input type="text" id="node"  disabled value="{{ $node->user_account }}" style="width: 75%"/>
            </div>
            <div class="form-item">
                <label for="phone">手机号:</label>
                <input type="text" id="phone"  placeholder="请输入您的手机号" name="user_phone" value="" style="width: 43%"/>
                <span style="color: #ff8300" onclick="send({{ auth::guard('app')->user()->user_phone}},{{ auth::guard('app')->user()->user_id }})"> 发送验证码</span>
            </div>
            <div class="form-item">
                <label for="name">验证码:</label>
                <input type="text" id="name" placeholder="请输入您的验证码" name="code" style="width: 75%"/>
                <input type="hidden" id="code" placeholder="请输入您的验证码" value="" style="width: 75%"/>
            </div>
            <div class="form-item">
                <label for="account">用户名:</label>
                <input type="text" id="account" name="user_account" placeholder="请输入您的用户名" value="" style="width: 75%"/>
            </div>
            <div class="form-item">
                <label for="pwd">密码:</label>
                <input type="password" id="pwd" name="user_pwd" value="" placeholder="请输入您的密码" style="width: 75%"/>
            </div>
            <button  class="btm" >确定</button>
        </form>
        </div>
        <div style="margin-bottom: 3.3rem"></div>
    </div>

@endsection
@section('footer')
@endsection
@section('js')
    <script>
        $('.updateSub').css('height',window.innerHeight+'px');
        function send(phone,uid){

            $.get('{{ url("/code") }}',{phone:phone,user_id:uid},function (data) {
                console.log(data)
                $('#code').val(data.code);
                layer.open({
                    content: '验证码已发送'
                    ,skin: 'msg'
                    ,time: 2 //2秒后自动关闭
                });

            });

        }
        $('form').submit(function () {
            var t = $(this).serializeArray();
            var d = {};
            $.each(t, function() {
                d[this.name] = this.value;
            });
            if(!d.code)
            {
                layer.open({
                    content: '请输入验证码'
                    ,btn: '我知道了'
                });
                return false
            }
            else {

                if (d.code != $('#code').val()) {
                    layer.open({
                        content: '您的验证码有误'
                        , btn: '我知道了'
                    });
                    return false
                }

            }

        })
    </script>

@endsection
