@extends('public.app.head')

@section('content')
    <div class="me">
        <header>
            <span onclick="window.history.back(-1)"></span>我的
        </header>
        <div style="height: 1.1rem"></div>
        <div class="me-user" style="background: url('{{ asset('image/mbg.png') }}') no-repeat ;background-size: 100%">
            <div class="me-role">
                <img src="{{ asset('image/role.png') }}" class=""/><span>VIP{{ $user->user_role }}</span>
            </div>
            <div class="me-info">
                <img src="{{ $user->user_photo }}" style="border-radius: 50%"/>
                <div>
                    <p>{{ $user->user_account }}</p>
                    <p>{{ $user->user_phone }}</p>
                </div>
            </div>
        </div>
        <div class="me-list" style="line-height: 3.5rem">
            <a href="{{ url('/app/me/msg') }}" class="list-item">
                <div>
                    <img src="{{ asset('image/zxly.png') }}">在线留言
                </div>
                <i></i>
            </a>
            <a href="{{ url('app/notify/list') }}" class="list-item">
                <div>
                    <img src="{{ asset('image/gg.png') }}">最新公告
                </div>
                <i></i>
            </a>
            <a href="{{ url('app/me/editPwd') }}" class="list-item">
                <div>
                    <img src="{{ asset('image/pwd.png') }}">修改密码
                </div>
                <i></i>
            </a>
            <a href="{{ url('app/me/out') }}" class="list-item">
                <div>
                    <img src="{{ asset('image/exit.png') }}">推出登陆
                </div>
                <i></i>
            </a>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $('.me').css('height',window.innerHeight+'px');
    </script>
@endsection
