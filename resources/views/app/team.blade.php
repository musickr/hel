@extends('public.app.head')
@section('style')
    <style>
        .team-item div:nth-child(2){
            border-left: 0.02rem solid #e2e3e5;
        }
    </style>
    @endsection
@section('content')
    <div class="team">
        <header>
            <span onclick="window.history.back(-1)"></span>我的团队
        </header>
        <div style="height: 1.1rem"></div>
        <div class="search">
            <input name="phone" type="text" value="" placeholder="请输入队员手机号">
            <img src="{{ asset('image/search.png') }}" onclick="search()">
        </div>
        <div class="msg">
            <ul class="team-msg">
                <li><i style="background: #183159"></i><p>直推</p><span>{{ $referrer }}</span></li>
                <li><i style="background: #f89600"></i><p>团队</p><span>{{ $team }}</span></li>
                <li><i style="background: #2c7e02"></i><p>业绩</p><span>{{ $user->property->sum('property_num') }}</span></li>
            </ul>
        </div>
        <div class="team-k">
            @if(!empty($child->toArray()))

                <div class="team-item">
                    @foreach($child as $c)
                    <div>
                        <p>{{ $c->num }}</p>
                        <span>{{ $c->child }}人</span>
                    </div>
                    @endforeach
                </div>
                @else
                    <div class="team-item" style="padding:0;color:#e2e3e5;width: 2rem;height: 2rem;flex-direction: column;align-items: center;background: #354060">
                        <p>{{ $user->user_account }}</p>
                        <p>VIP{{ $user->user_role }}</p>
                    </div>
                @endif
            <span style="display:inline-block;width: 0.02rem;height: 1rem;background: #354060"></span>
            <div style="width: 50%;height: 1rem;border: 0.02rem solid #354060;border-bottom: none"></div>
            <div style="display: flex;justify-content: space-around;width: 100%">
                @if(!empty($child->toArray()))
                    @foreach($child as $c)
                <div class="team-item" style="padding:0;color:#e2e3e5;width: 2rem;height: 2rem;flex-direction: column;align-items: center;background: #354060">
                    <p>{{ $c->user_account }}</p>
                    <p>VIP{{ $user->user_role }}</p>
                </div>
                    @endforeach
                @elseif(count($child->toArray())<2)
                    @for($i=0;$i<2-count($child->toArray());$i++)
                    <div onclick="register({{ $user->user_id }},{{ $user->user_role }})" class="team-item" style="padding:0;color:#e2e3e5;width: 2rem;height: 2rem;flex-direction: column;align-items: center;background: #354060">
                        <p>注册</p>
                    </div>
                    @endfor
                @endif
            </div>

        </div>

    </div>

@endsection
@section('js')
    <script>
        $('.team').css('height',window.innerHeight+'px');
        //注册
        function register(uid,role) {
            !role
                ? layer.open({content: '权限不足，请升级！',skin: 'msg',time: 1})
                : window.location.href =  '{{ url("app/team/register/") }}'+"/"+uid;
        }
        function search() {
            let user_phone= $('input[name="phone"]').val();
            if (user_phone.length == 0) {
               console.log('手机号没有输入');
            } else {
                if(isPhoneNo(user_phone) == false) {
                   console.log('手机号码不正确');
                }
                else{
                    window.location.href = '{{ url("app/team/search") }}'+ '/' + user_phone;
                }
            }
        }
        function isPhoneNo(phone) {
            var pattern = /^1[34578]\d{9}$/;
            return pattern.test(phone);
        }
    </script>
@endsection
