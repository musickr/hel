@extends('public.app.head')
@section('style')
    <style>
        a,a:hover,a:active,a:visited,a:link,a:focus{

            outline:none;
            color: #ffffff;
            text-decoration: none;
        }
        .btm {
            outline: none;
            text-decoration: none;
            padding: 0;
            line-height: 1rem;

            height: 1rem;
            width: 5.95rem;
            margin: 0.45rem auto;
            background: #183159;
            color: #ffffff;
            border: none;
            font-size: 0.3rem;
            display: block;
            border-radius: 0.15rem;
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <div class="goldCoinDetail">
        <header>
            <span onclick="window.history.back(-1)"></span>金币转账
        </header>
        <div style="height: 1.1rem"></div>
        <form method="post" action="{{ url('app/Coin/transfer') }}">
            @csrf
            <div class="goldCoinDetail-item">
                <input type="text" name="to_uid" value="" placeholder="请输入对方账号" style="border: none;background: none;outline: none;text-align: center;width: 100%">
            </div>
            <div class="goldCoinDetail-item">
                <input type="text" name="property_num" value="" placeholder="请输入转币金额" style="border: none;background: none;outline: none;text-align: center;width: 100%">
            </div>
            <small style="display: block;width:5.9rem;text-align: right ;margin: 0.15rem auto">当前金币数量    <span style="color: #fe0000">{{ $num }}</span></small>
            <button class="btm">确定</button>
        </form>
        <div style="margin-bottom: 3.3rem"></div>
    </div>

@endsection
@section('footer')
    @endsection
@section('js')
    <script>
        $('.goldCoinDetail').css('height',window.innerHeight+'px');
    </script>
@endsection
