@extends('public.app.head')
@section('style')
    <style>
        a,a:hover,a:active,a:visited,a:link,a:focus{

            outline:none;
            color: #ffffff;
            text-decoration: none;
        }
        .btm {
            outline: none;
            text-decoration: none;
            padding: 0;
            line-height: 1rem;

            height: 1rem;
            width: 5.95rem;
            margin: 0.45rem auto;
            background: #183159;
            color: #ffffff;
            border: none;
            font-size: 0.3rem;
            display: block;
            border-radius: 0.15rem;
            text-align: center;
        }
        input{
            font-size: 0.3rem;
        }
    </style>
@endsection
@section('content')
    <div class="updateSub">
        <header>
            <span onclick="window.history.back(-1)"></span>修改收款信息
        </header>
        <div style="height: 1.1rem"></div>
        <div class="pay-msg">
            <form action="{{ url('app/pay/edit') }}" method="post" style="width: 85%;margin-top: 0.5rem">
                @csrf
            <div class="form-item">
                <label for="name">开户姓名:</label>
                <input type="text" id="name" name="ac_name" value="{{ $msg->ac_name }}" style="width: 75%"/>
            </div>
            <div class="form-item">
                <label for="bank">开户银行:</label>
                <input type="text" id="bank" name="ac_bank_name" value="{{ $msg->ac_bank_name }}" style="width: 75%"/>
            </div>
            <div class="form-item">
                <label for="num">银行卡号:</label>
                <input type="text" id="num" name="ac_bank_num" value="{{ $msg->ac_bank_num }}" style="width: 75%"/>
            </div>
            <div class="form-item">
                <label for="z">支付宝:</label>
                <input type="text" id="z" name="ac_zpay" value="{{ $msg->ac_zpay }}" style="width: 75%"/>
            </div>
            <div class="form-item">
                <label for="w">微信账号:</label>
                <input type="text" id="w" name="ac_wechat" value="{{ $msg->ac_wechat }}" style="width: 75%"/>
            </div>
            <div class="form-item">
                <input type="text" id="name" name="code" value="" placeholder="请输入验证码" style="width: 75%"/>
                <input type="hidden" id="code" name="" value="" style="width: 75%"/>
                <span style="color: #ff8300" onclick="send({{ auth::guard('app')->user()->user_phone}},{{ auth::guard('app')->user()->user_id }})">发送验证码</span>
            </div>
                <input type="hidden" name="ac_id" value="{{ $msg->ac_id }}">
                <input type="hidden" name="ac_uid" value="{{ $msg->ac_uid }}">
            <button class="btm" >确定</button>
        </form>
        </div>
        <div style="margin-bottom: 3.3rem"></div>
    </div>

@endsection
@section('js')
    <script>
        $('.updateSub').css('height',window.innerHeight+'px');
        function send(phone,uid){

            $.get('{{ url("/code") }}',{phone:phone,user_id:uid},function (data) {
                console.log(data)
                $('#code').val(data.code);
                layer.open({
                    content: '验证码已发送'
                    ,skin: 'msg'
                    ,time: 2 //2秒后自动关闭
                });

            });

        }
        $('form').submit(function () {
            var t = $(this).serializeArray();
            var d = {};
            $.each(t, function() {
                d[this.name] = this.value;
            });
            if(!d.code)
            {
                layer.open({
                    content: '请输入验证码'
                    ,btn: '我知道了'
                });
                return false
            }
            else {

                if (d.code != $('#code').val()) {
                    layer.open({
                        content: '您的验证码有误'
                        , btn: '我知道了'
                    });
                    return false
                }

            }

        })

    </script>
@endsection
