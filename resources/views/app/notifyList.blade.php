@extends('public.app.head')
@section('style')
    <style>
        a:active {
            outline: none;
            text-decoration: none;
        }

        a {
            text-decoration: none;
            color: #333;
            border-radius: 0;
            width: 100%;
        }

        p {
            width: 100%;
            font-size: 0.25rem;
            margin-top: 0.2rem;
            text-indent: 0.6rem;
            text-align: justify !important;
            line-height: 1.5;
            overflow: hidden;
            /*text-overflow: ellipsis;
            white-space: nowrap;*/
            display: -webkit-box;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
        }
        span{
            flex: 1;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
    </style>
@endsection
@section('content')
    <div class="goldCoinDetail" style="background: #ffffff">
        <header>
            <span onclick="window.history.back(-1)"></span>最新公告
        </header>
        <div style="height: 1.1rem"></div>
        <div style="display: flex;justify-content: flex-start;flex-direction:column;width: 100%">
            @foreach($notifys as $notify )
            <a href="{{ url('app/notify/'.$notify->notify_id) }}">
                <div style="width: 90%;padding: 0.4rem 0 0.2rem 0;margin:0 auto;border-bottom: 0.02rem solid #f8f8f8">
                    <h1 style="display: flex;justify-content: space-between;align-items:flex-end;font-size: 0.35rem;font-weight: 600">
                        <span>{{ $notify->notify_title }}</span>
                        <small style="font-weight:400;font-size: 0.25rem;color: #999999">{{ $notify->created_at }}</small>
                    </h1>
                    <p style="">
                        {{ $notify->notify_content }}
                    </p>
                </div>
            </a>
            @endforeach
        </div>
        <div style="margin-bottom: 3.3rem"></div>
    </div>

@endsection
@section('footer')
@endsection
@section('js')
    <script>
        $('.goldCoinDetail').css('height', window.innerHeight + 'px');
    </script>
@endsection
