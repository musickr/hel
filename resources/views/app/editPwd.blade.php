@extends('public.app.head')
@section('style')
    <style>
        a,a:hover,a:active,a:visited,a:link,a:focus{

            outline:none;
            color: #ffffff;
            text-decoration: none;
        }
        .btm {
            outline: none;
            text-decoration: none;
            padding: 0;
            line-height: 1rem;

            height: 1rem;
            width: 5.95rem;
            margin: 0.45rem auto;
            background: #183159;
            color: #ffffff;
            border: none;
            font-size: 0.3rem;
            display: block;
            border-radius: 0.15rem;
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <div class="goldCoinDetail">
        <header>
            <span onclick="window.history.back(-1)"></span>修改密码
        </header>
        <div style="height: 1.1rem"></div>
        <form method="post" action="{{ url('app/me/edit') }}">
            @csrf
            <div class="goldCoinDetail-item">
                <div class="text" style="width: 100%">
                    <p>验证码</p>
                    <p style="display: flex;justify-content: space-between;align-items: center;width: 100%">
                        <input type="text" name="code" value="" style="border: none;background: none;outline: none;" placeholder="请输入验证码"/>
                        <input type="hidden" id="code" value="">
                        <span style="font-size: 0.3rem;color: #ff8500" onclick="send({{ auth::guard('app')->user()->user_phone}},{{ auth::guard('app')->user()->user_id }})">发送验证码</span>
                    </p>
                </div>
            </div>
            <div class="goldCoinDetail-item">
                <div class="text" style="width: 100%">
                    <p>新密码</p>
                    <p style="display: flex;justify-content: space-between;align-items: center;width: 100%">
                        <input type="password" name="user_pwd" value="" style="border: none;background: none;outline: none;" placeholder="请设置新密码"/>
                    </p>
                </div>
            </div>
            <div class="goldCoinDetail-item">
                <div class="text" style="width: 100%">
                    <p>确认密码</p>
                    <p style="display: flex;justify-content: space-between;align-items: center;width: 100%">
                        <input type="password" name="c_pwd" value="" style="border: none;background: none;outline: none;" placeholder="请输入您的新密码"/>
                    </p>
                </div>
            </div>
            <button class="btm" >确定</button>
        </form>
        <div style="margin-bottom: 3.3rem"></div>
    </div>

@endsection
@section('js')
    <script>
        $('.goldCoinDetail').css('height',window.innerHeight+'px');
        function send(phone,uid){

            $.get('{{ url("/code") }}',{phone:phone,user_id:uid},function (data) {
                console.log(data)
                $('#code').val(data.code);
                layer.open({
                    content: '验证码已发送'
                    ,skin: 'msg'
                    ,time: 2 //2秒后自动关闭
                });
            });

        }
        $('form').submit(function () {
            var t = $('form').serializeArray();
            var d = {};
            $.each(t, function() {
                d[this.name] = this.value;
            });
            if(!d.code)
            {
                layer.open({
                    content: '请输入验证码'
                    ,btn: '我知道了'
                });
            }
            else
            {

                if(d.code != $('#code').val())
                {
                    layer.open({
                        content: '您的验证码有误'
                        ,btn: '我知道了'
                    });
                }
                else
                {
                    if(!d.user_pwd)
                    {
                        layer.open({
                            content: '密码不能为空'
                            ,btn: '我知道了'
                        });
                    }
                    else
                    {
                        if(d.user_pwd != d.c_pwd )
                        {
                            layer.open({
                                content: '两次密码不一致'
                                ,btn: '我知道了'
                            });
                            return false
                        }

                    }

                }
            }



        })
    </script>
@endsection
