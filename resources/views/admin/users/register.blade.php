@extends('admin.public.app')
@section('content')
    <form action="" class="layui-form" method="post">
        <div class="layui-form-item">
            <label class="layui-form-label" for="account">用户名：</label>
            <div class="layui-input-block">
                <input class="layui-input" id="account" type="text" value="" name="user_account" placeholder="请输入用户名" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" for="password">密码：</label>
            <div class="layui-input-block">
                <input class="layui-input" id="password" type="password" value="" name="user_pwd" placeholder="请输入密码" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" for="phone">手机号码</label>
            <div class="layui-input-block">
                <input class="layui-input" id="phone" type="text" value="" name="user_phone" placeholder="请输入用户手机号" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" for="account">推荐人</label>
            <div class="layui-input-block">
                <input class="layui-input" id="account" type="text" value="" name="user_account" placeholder="请输入用户名" />
            </div>
        </div>
    </form>

@endsection
@section('js')
    <script>
        layui.use('table', function(){
            $ = layui.jquery;
        });
    </script>
@endsection
