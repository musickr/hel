@extends('admin.public.app')
@section('content')
    <div class="layui-form" style="display: flex;align-items: center">
        <div class="layui-input-inline">
            <input name="search" id="search" value="" style="display: inline-block;width: unset" placeholder="请输入... ..." class="layui-input">
            {{--<button class="layui-btn"><i class="fa fa-search"></i></button>--}}
        </div>
    </div>
    <table id="users" lay-filter="users"></table>
    <script type="text/html" id="toolBar">
        <a class="layui-btn layui-btn-xs" lay-event="recharge">充值</a>
        <a class="layui-btn layui-btn-xs" lay-event="del">删除</a>
    </script>

@endsection
@section('js')
    <script>
        layui.use(['table','layer'], function(){
            var table = layui.table;
            var layer = layui.layer;
            var $     = layui.jquery;
            //列表
            table.render({
                elem: '#users'
                ,height: ''
                ,url: '{{ url('/api/admin/users/list') }}' //数据接口
                /*,parseData:function (res) {
                    console.log(res);
                    /!*return{
                        "code":0,
                        "msg":
                    }*!/
                }*/

                ,defaultToolbar:[]
                ,page: true //开启分页
                ,cols: [[ //表头
                    {field: 'user_id', title: 'ID', width:80, fixed: 'left'}
                    ,{field: 'user_account', title: '用户名',edit:'text'}
                    ,{field: 'user_phone', title: '手机',edit:'text' }
                    ,{field: 'user_ref', title: '推荐人'}
                    ,{field: 'user_node', title: '节点人'}
                    ,{field: 'user_role', title: '等级',edit:'text'}
                    ,{field: 'user_coin', title: '资产'}
                    ,{field: 'created_at', title: '注册时间'}
                    ,{field: 'wealth', title: '操作',toolbar: '#toolBar',fixed: 'right'}

                ]]
            });

            //监听行工具事件
            table.on('tool(users)', function(obj){
                var data = obj.data;
                //console.log(obj.data);
                switch (obj.event) {
                    case 'recharge':
                        parent.lay(data.user_id);
                        break;
                    case 'del':

                        $.post('{{ url("api/admin/user/del") }}',{uid:data.user_id},function (data) {
                            console.log(data);
                            layer.msg(data.msg);
                            table.reload('users');
                        });
                        break

                }

            });

            //监听单元格编辑
            table.on('edit(users)', function(obj){
                var value = obj.value //得到修改后的值
                    ,data = obj.data //得到所在行所有键值
                    ,field = obj.field; //得到字段
                var date = {
                    uid:data.user_id,
                    field:field,
                    value:value
                };
                $.post('{{ url("api/admin/user/edit") }}',date,function (data) {
                    layer.msg(data.msg);
                    table.reload('users')
                });

            });

            //搜索
            $('#search').keyup(function(){
                var data = {
                    search:$(this).val()
                };
                table.reload('users', {
                    url: '{{ url("api/admin/users/search") }}'
                    ,where: data
                    ,method:'post'//设定异步数据接口的额外参数
                    //,height: 300
                });
                /*$.post("{{ url('api/admin/users/search') }}",data,function (data) {
                    console.log(data);
                })*/
            })
        });
    </script>
@endsection