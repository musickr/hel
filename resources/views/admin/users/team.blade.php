@extends('admin.public.app')
@section('content')

    <form class="layui-form" action="">
        <div class="layui-form-item">
            <div class="layui-inline">
                <div class="layui-input-inline">
                    <input class="layui-input" value="" name="user_phone" placeholder="请输入用户电话">
                </div>
                <div class="layui-input-inline">
                    <button class="layui-btn" lay-submit=""  type="button" lay-filter="search" style="display: inline-block">搜索</button>
                </div>
            </div>
        </div>
    </form>
    <ul id="tree"></ul>

@endsection
@section('js')
    <script>
        layui.use(['element','tree','form'], function() {
            var $ = layui.jquery;
            var element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
            let tree = layui.tree;
            let form = layui.form;

            $.get('{{url("api/admin/user/team?user_phone=")}}',function (data) {
                layui.tree({
                    elem: '#tree'
                    ,nodes: data
                });
            },'json');
            form.on('submit(search)',function (data) {

                $.get('{{url("api/admin/user/team")}}',data.field,function (data) {
                    console.log(data);
                    if(data)
                    {
                        layui.tree({
                            elem: '#tree'
                            ,nodes: data
                        });
                    }

                },'json');
            })


        });
    </script>
@endsection
