@extends('layouts.app')
@section('style')
    <style>
        .list-group-item:first-child,.list-group-item:last-child{
            border-radius: 0;
        }
    </style>
    @endsection
@section('content')
<div class="container-fluid" style="margin-top: 0.1rem;width: 100%">
    <div class="row justify-content-center" style="height: 100%;text-align: center">
        @if(isset($menus) && !empty($menus))
        <div class="col-2" style="background:#FFFFFF;height: 100%;padding: 0;margin: 0">
            <div id="accordion" class="list-group">
                @foreach( $menus as $menu )
                    <p class="list-group-item list-group-item-action {{--@if($menu->menu_id==1)active @endif--}}"   data-toggle="collapse" data-target="#{{ $menu['menu_name'] }}" aria-expanded="true" role="button">
                        <i class="fa fa-{{ $menu['menu_icon'] }} "></i>
                        {{ $menu['menu_name'] }}
                    </p>

                    @if(isset($menu['menu_child']))
                    <div class="list-group collapse" id="{{ $menu['menu_name'] }}"   data-parent="#accordion">
                        @foreach($menu['menu_child'] as $child)
                        <a class="list-group-item list-group-item-action sub" route="{{ url($child['menu_route']) }}"  data-toggle="list">
                            <i style="margin-left: 1rem" class="fa fa-{{ $child['menu_icon'] }} "></i>
                            {{ $child['menu_name'] }}
                        </a>
                        @endforeach
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
        @endif
        <div class="col-10" style="height: 93%;    margin-top: 0.8rem;">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a  td="#collapseTwo" id="one">Home</a></li>
                    {{--<li class="breadcrumb-item"><a href="#">Library</a></li>--}}
                    {{--<li class="breadcrumb-item active" aria-current="page">Data</li>--}}
                </ol>
            </nav>
            <iframe src='{{ url('admin/index') }}' width='100%'  frameborder='0' scrolling="no" name="_blank" id="_blank" ></iframe>
        </div>
    </div>
</div>
{{--充值弹窗--}}
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">用户充值</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" >
                    <div class="form-group">
                        <label for="property_num" class="col-form-label">充值金额</label>
                        <input type="number" class="form-control" name="property_num" id="property_num">
                        <input type="hidden" name="property_uid" id="property_uid" value="">
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" id="recharge" class="btn btn-secondary">提交</button>
                <button type="reset" class="btn btn-primary" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>


{{--充值弹窗--}}
<div class="modal fade" id="transfer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">转账</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" >
                    <div class="form-group">
                        <label for="property_out" class="col-form-label">对方ID</label>
                        <input type="number" class="form-control" name="property_uid" id="property_out">

                        <label for="property_inNum" class="col-form-label">转账金额</label>
                        <input type="number" class="form-control" name="property_num" id="property_inNum">

                        <input type="hidden" name="property_in" id="property_inId" value="">
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" id="transfered" class="btn btn-secondary">提交</button>
                <button type="reset" class="btn btn-primary" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

{{--消息弹窗--}}
<div class="modal fade" id="msg" tabindex="-1" role="dialog" aria-labelledby="msgModal" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="card">
                <div class="card-body" id="mgs-content">

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('js')
    <script>
        var ifm= document.getElementById("_blank");
        ifm.height=document.documentElement.clientHeight*0.85;
        $(function () {
            $('p').click(function () {
                console.log()
                $('#one').text($(this).text());
                $('#one').attr('td',$(this).attr('data-target'))
            });
            $('.sub').click(function () {

                $('iframe').attr('src',$(this).attr('route'))
                /*$($(this).attr("td")).collapse({
                    toggle: true
                })*/
            });

            $('#recharge').click(function () {
                let data;
                data = {
                    property_uid:$("#property_uid").val(),
                    property_num:$("#property_num").val()
                }

                if($("#property_num").val())
                {
                    console.log(data);
                    $.post('{{ url('api/admin/user/recharge') }}',data,function (date) {
                        /*console.log(date)*/
                        if(data)
                        {
                            $('#exampleModal').modal('hide');
                        }
                    })
                }
            })

            //转账
            $('#transfered').click(function  transfered() {
                let data;
                data = {
                    property_uid:$("#property_out").val(),
                    property_num:$("#property_inNum").val(),
                    property_in:$("#property_inId").val()
                };

                if($("#property_inNum").val())
                {
                    console.log(data);
                    $.post('{{ url('api/admin/user/transfer') }}',data,function (date) {
                        console.log(date)
                        if(date.status == 'error')
                        {
                            $('#transfer').modal('hide');
                            $('#mgs-content').text(date.msg);
                            $('#msg').modal('show');
                            setTimeout(function (){$('#msg').modal('hide')},1000);
                        }
                        if(date.status == 'success')
                        {
                            $('#transfer').modal('hide');
                            $('#mgs-content').text(date.msg);
                            $('#msg').modal('show');
                            setTimeout(function (){$('#msg').modal('hide')},1000);
                        }
                    })
                }
            })
        });
        function lay(uid) {
            $('#exampleModal').modal('show');
            console.log(uid);
            $('#property_uid').val(uid);

        }
        function transfer(uid) {
            $('#property_inId').val(uid);
            $('#transfer').modal('show');
        }
    </script>
@endsection
