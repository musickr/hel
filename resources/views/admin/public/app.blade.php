<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- layui -->
    <link href="{{ asset('layui/css/layui.css') }}" rel="stylesheet">

    {{-- font_icon --}}
    <link rel="stylesheet" href="{{ asset('font/css/font-awesome.min.css') }}">
</head>
@yield('style')
<body>

<main>
    @yield('content')
</main>
<script src="{{ asset('layui/layui.js') }}" ></script>
<script>


</script>
@yield('js')
</body>
</html>
