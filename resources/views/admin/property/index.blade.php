@extends('admin.public.app')
@section('content')
    <table id="propertys" lay-filter="propertys"></table>
    <script type="text/html" id="toolBar">
        {{--<a class="layui-btn layui-btn-xs" lay-event="recharge">转账</a>--}}
    </script>

@endsection
@section('js')
    <script>
        layui.use(['table','layer'], function(){
            var table = layui.table;
            var layer = layui.layer;
            var layer = parent.layer === undefined ? layui.layer : parent.layer;
            //列表
            table.render({
                elem: '#propertys'
                ,height: ''
                ,url: '{{ url('/api/admin/propertys/list') }}' //数据接口
                /*,parseData:function (res) {
                    console.log(res);
                    /!*return{
                        "code":0,
                        "msg":
                    }*!/
                }*/

                ,defaultToolbar:[]
                ,page: true //开启分页
                ,cols: [[ //表头
                    {field: 'id', title: 'ID', width:80, fixed: 'left'}
                    ,{field: 'property_user', title: '用户名' }
                    ,{field: 'property_in', title: '资金来源' }
                    ,{field: 'property_type', title: '资金类型'}
                    ,{field: 'property_num', title: '金额'}
                    ,{field: 'property_remark', title: '备注'}
                    ,{field: 'created_at', title: '交易时间'}
                    /*,{field: 'wealth', title: '操作',toolbar: '#toolBar',fixed: 'right'}*/

                ]]
            });

            //监听行工具事件
            table.on('tool(propertys)', function(obj){
                var data = obj.data;
                //console.log(obj.data);
                switch (obj.event) {
                    case 'recharge':
                        parent.transfer(data.property_uid);

                        break;

                }

            });

        });
    </script>
@endsection
