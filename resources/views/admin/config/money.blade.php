@extends('admin.public.app')
@section('content')
    <table id="config" lay-filter="config"></table>
    <script type="text/html" id="toolBar">
        <a class="layui-btn layui-btn-xs" lay-event="recharge">转账</a>
    </script>

@endsection
@section('js')
    <script>
        layui.use(['table','layer'], function(){
            var table = layui.table;
            var layer = layui.layer;
            var $     = layui.jquery;
            //列表
            table.render({
                elem: '#config'
                ,height: ''
                ,url: '{{ url('/api/admin/system/money') }}' //数据接口
                /*,parseData:function (res) {
                    console.log(res);
                    /!*return{
                        "code":0,
                        "msg":
                    }*!/
                }*/

                ,defaultToolbar:[]
                ,cols: [[ //表头
                    {field: 'config_name', width:'50%',title: '配置项' }
                    ,{field: 'config_value', width:'50%', title: '值',edit:'text'}
                ]]
            });

            //监听单元格编辑
            table.on('edit(config)', function(obj){


                var value = obj.value //得到修改后的值
                    ,data = obj.data //得到所在行所有键值
                    ,field = obj.field; //得到字段

                console.log(data);
                var date = {
                    id:data.id,
                    config_value:data.config_value,
                };
                console.log(date);
                $.post('{{ url("api/admin/system/money/edit") }}',date,function (data) {
                    layer.msg(data.msg);
                    table.reload('config')
                });

            });

        });
    </script>
@endsection
