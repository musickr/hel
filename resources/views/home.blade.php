@extends('layouts.app')
@section("style")
    <style>
        .list-group-item:first-child{
            border-radius: 0;
        }
        iframe{
            /*height: 900px !important;*/
        }
    </style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-2" style="padding-left: 0;text-align: center">
            <div id="accordion">
                @foreach( $menus as $menu )
                    <div class="list-group-item"  id="heading{{ $menu['menu_name'] }}" data-toggle="collapse" data-target="#collapse{{ $menu['menu_name'] }}" aria-expanded="true" aria-controls="collapseOne">
                        <i class="fa fa-{{ $menu['menu_icon'] }} "></i>    {{ $menu['menu_name'] }}
                    </div>
                    @if(isset($menu['menu_child']))
                        <div id="collapse{{ $menu['menu_name'] }}" class="collapse" aria-labelledby="heading{{ $menu['menu_name'] }}" data-parent="#accordion">
                            <div class="list-group menu" id="list-tab" role="tablist">
                                @foreach($menu['menu_child'] as $child)
                                    @if($child['menu_show'])
                                        <a class="list-group-item list-group-item-action" id="" data-toggle="list" href="" route="{{ url($child['menu_route']) }}" role="tab" aria-controls="home">
                                            <i style="margin-left: 1rem" class="fa fa-{{ $child['menu_icon'] }} "></i>    {{ $child['menu_name'] }}
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @if(isset($menu['menu_child']))
                @endforeach
            </div>
        </div>
        <div class="col-md-10" style="padding: 0;height: 100%">
            <div class="card" style="height: 100%">
                <div class="tab-content" id="nav-tabContent" style="height: 100%">
                    <div class="tab-pane fade show active" id="content" role="tabpanel" aria-labelledby="list-home-list" style="height: 100%">
                        <iframe id="myiframe" src="http://www.baidu.com"  width='100%' height='100%' frameborder='0' name="_blank" id="_blank"  ></iframe>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
    @section('js')
        <script>
            $(document).ready(
                function () {
                    $('.menu a').click(function () {
                        var route = $(this).attr('route');
                        console.log(route);
                        $('iframe').attr('src',route);
                    })
                }
            )
        </script>
        @endsection
@endsection
