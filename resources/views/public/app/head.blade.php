<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{--<link rel="stylesheet" href="http://at.alicdn.com/t/font_913737_nxeovkxs86.css">--}}
        <script type="" src="{{ asset('js/myApp.js') }}"></script>

       {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}">--}}
        <link rel="stylesheet" href="{{ asset('css/reset.css') }}">
        <link rel="stylesheet" href="{{ asset('css/myApp.css') }}">
        <link rel="stylesheet" href="{{ asset('layer_mobile/layer_mobile/need/layer.css') }}">
        <script type="" src="{{ asset('js/clipboard.js') }}"></script>

        <title></title>

    </head>

    @section('style')

    @show
    <body>
        @section('content')
            <header>
                <span onclick="window.history.back(-1)"></span>首页
            </header>
        @show
        @section('footer')
            <footer>
                <div class="footer">
                    <a href="{{ url('/app/index/index') }}" class="tab-index">
                        <img <?php if(explode('/',url()->current())[4] == 'index'){?>src="{{ asset('image/home-1.png') }}"<?php }else{?> src="{{ asset('image/home-0.png') }}" <?php }?> class="tab-icon">
                        <p class="tab-title">首页</p>
                    </a>

                    <a href="{{ url('/app/team/index') }}" class="tab-index">
                        <img <?php if(explode('/',url()->current())[4] == 'team'){?>src="{{ asset('image/team-1.png') }}"<?php }else{?> src="{{ asset('image/team-0.png') }}" <?php }?> class="tab-icon">
                        <p class="tab-title">团队</p>
                    </a>

                    <a href="{{ url('app/me/index') }}" class="tab-index">
                        <img <?php if(explode('/',url()->current())[4] == 'me'){?>src="{{ asset('image/me-1.png') }}"<?php }else{?> src="{{ asset('image/me-0.png') }}" <?php }?> class="tab-icon">
                        <p class="tab-title">我的</p>
                    </a>
                </div>
            </footer>
        @show
    </body>
    <script type="" src="{{ asset('js/jq-3.3.1.min.js') }}"></script>

    <script type="" src="{{ asset('layer_mobile/layer_mobile/layer.js') }}"></script>



    @section('js')


    @show

</html>
