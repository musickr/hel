<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCoin extends Model
{
    //
    protected $table = 'app_user_coin';
    protected $fillable = [
        'coin_num',
        'coin_type',
        'coin_in',
        'coin_out'
    ];
    protected $hidden = [
        'user_pwd'
    ];
}
