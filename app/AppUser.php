<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AppUser extends Authenticatable
{
    //
    use Notifiable;
    protected $table = 'app_user';
    protected $primaryKey = 'user_id';
    protected $fillable = [
        'user_account',
        'user_pwd',
        'user_role',
        'user_referrer',
        'user_pid',
        'user_photo',
        'user_phone',
    ];
    protected $hidden = [
        'user_pwd'
    ];
    public function getAuthPassword() {
        return $this->user_pwd;        // 一定要返回加密了的密码
    }
    public function coins()
    {
        return $this->hasMany('App\UserCoin','id','user_id');
    }
    public function account()
    {
        return $this->hasMany('App\UserAccount','ac_uid','user_id');
    }
    public function property()
    {
        return $this->hasMany('App\Property','property_uid','user_id');
    }
    public function setUserPwdAttribute($value)
    {
        $this->attributes['user_pwd'] = c($value);
    }
    public function massages()
    {
        return $this->hasMany('App\Massage','msg_uid','user_id');
    }
}
