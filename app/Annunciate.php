<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Annunciate extends Model
{
    //
    protected $table = 'app_notify';
    protected $primaryKey = 'notify_id';
    protected $fillable = [
        'notify_title',
        'notify_content'
    ];
    public function getCreatedAtAttribute()
    {
        return date('Y/m/d H:i', strtotime($this->attributes['created_at']));
    }
}
