<?php

namespace App\Http\Middleware;

use Closure;

class AuthAppUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->guard('app')->guest())
        {
            return redirect()->guest('app/index');
        }
        return $next($request);
    }
}
