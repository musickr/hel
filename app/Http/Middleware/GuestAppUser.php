<?php

namespace App\Http\Middleware;

use Closure;

class GuestAppUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->guard('app')->check()) {
            return redirect('/app/index/index');
        }
        return $next($request);
    }
}
