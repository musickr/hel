<?php

namespace App\Http\Controllers\Admin\Api;


use App\Http\Resources\UserPropertyResource;
use App\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class PropertyController extends Controller
{
    public function list(Request $request){
        $page = $request->get('page');
        $limit = $request->get('limit');
        $propertys = Property::all();

        $pageData =  new LengthAwarePaginator(
            array_slice($propertys->toArray(),($page-1)*$limit,$limit),
            $limit,
            $page);
        return response(new UserPropertyResource($pageData),200);
    }
}
