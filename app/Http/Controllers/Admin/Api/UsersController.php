<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/17
 * Time: 10:49
 */

namespace App\Http\Controllers\Admin\Api;


use App\AppUser;
use App\Http\Controllers\Web\View\CodeController;
use App\Http\Resources\AppUserResource;
use App\Property;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class UsersController extends CodeController
{
    public function list(Request $request){

        $page = $request->get('page');
        $limit = $request->get('limit');
        $users = AppUser::all();



        $pageData =  new LengthAwarePaginator(
            array_slice($users->toArray(),($page-1)*$limit,$limit),
            $limit,
            $page);
        return response(new AppUserResource($pageData),200);
    }
    public function recharge(Request $request)
    {
        $data = [
            'property_in'=>0,
            'property_uid'=>$request->post('property_uid'),
            'property_num' => $request->post('property_num'),
            'property_type' => 3,
            'property_remark' => '系统充值',
        ];
        $property = Property::query()
            ->create($data);
        if($property){
            return response()->json(['status'=>'充值成功'],200);
        }
    }
    //转账
    public function transfer(Request $request)
    {
        //非法操作
        if($request->post('property_in')==$request->post('property_uid'))
        {
            return response()->json(['msg'=>'非法操作','status'=>'error'],200);
        }
        //用户不存在
        $user = AppUser::query()->find($request->post('property_uid'));
        if(!$user)
        {
            return response()->json(['msg'=>'用户不存在','status'=>'error'],200);
        }
        //用户余额
        $RemainingBalance = $this->RemainingBalance($request->post('property_in'));

        if($RemainingBalance < $request->post('property_num'))
        {
            return response()->json(['msg'=>'余额不足','status'=>'error'],200);
        }

        //开始转账
        $data = [
            'property_in'=>$request->post('property_in'),
            'property_uid'=>$request->post('property_uid'),
            'property_num' => $request->post('property_num'),
            'property_type' => 2,
            'property_remark' => '收到'.AppUser::query()->find($request->post('property_in'))->value('user_account').'的转账',
        ];

        $property = Property::query()
            ->create($data);

        $data = [
            'property_in'=>$request->post('property_uid'),
            'property_uid'=>$request->post('property_in'),
            'property_num' => -$request->post('property_num'),
            'property_type' => 2,
            'property_remark' => '转账给'.$user->user_account,
        ];

        $property = Property::query()
            ->create($data);
        if($property){
            return response()->json(['status'=>'success','msg'=>'转账成功'],200);
        }

    }
    //查询用户余额
    public function RemainingBalance($uid)
    {
        return Property::query()->where('property_uid',$uid)->sum('property_num');

    }
    public function tree($rs,$tui)
    {
        $tree = [];
        foreach ($rs as $i => $r)
        {
            //$tree[] = $r;
            if($r->user_pid == $tui)
            {
                $r->children = $this->tree($rs,$r->user_id);
                $tree[] = $r;
            }

        }
        return $tree;
    }

    //用户团队
    public function team(Request $request)
    {
        $allUser = AppUser::all(['user_account as name','user_pid','user_id','user_phone']);
        foreach ($allUser as $item => $value)
        {
            $value['name'] = $value['name'].'('.$value['user_phone'].')';
            $allUser[$item] = $value;
        }

        $res = AppUser::where('user_phone','=',$request->get('user_phone'))->first();

        $tui = !empty($res)
            ? $res->user_id
            : null;

        $userTree = $this->tree($allUser,$tui);

        return response()->json($userTree,200);
    }
    //编辑
    public function edit(Request $request)
    {
        $res = AppUser::query()
            ->where('user_id',$request->post('uid'))
            ->update([
                $request->post('field')=>$request->post('value')
            ]);
        return response()->json(['status'=>$res,'msg'=>'修改成功'],200);
    }
    //删除
    public function del(Request $request)
    {
        $res = AppUser::query()->where('user_pid',$request->post('uid'))->get()->toArray();
        if(!empty($res))
        {
            return response()->json(['msg'=>'不能删除','status'=>'success'],200);
        }
        else
        {
            AppUser::query()->where('user_id',$request->post('uid'))->delete();
            return response()->json(['msg'=>'删除成功','status'=>'success'],200);
        }
    }
    //搜索
    public function search(Request $request)
    {
        $echostr = $request->post('search');
        $users   = AppUser::query()
        ->where('user_account',$echostr)
            ->orWhere('user_account','like','%'.$echostr.'%')
            ->orWhere('user_phone','like','%'.$echostr.'%')
            ->get();
        return response()->json(new AppUserResource($users),200);


    }
}
