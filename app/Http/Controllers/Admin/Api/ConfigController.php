<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/17
 * Time: 10:49
 */

namespace App\Http\Controllers\Admin\Api;


use App\AppConfig;
use App\AppUser;
use App\Http\Resources\AppConfigResource;
use App\Http\Resources\AppUserResource;
use App\Property;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class configController
{
    public function list(Request $request){

    }
    public function money(AppConfig $appConfig)
    {
        return response()->json(new AppConfigResource($appConfig::query()->where('config_type',1)->get()),200);
    }
    public function editMoney(Request $request)
    {
        AppConfig::query()
            ->where('id',$request->post('id'))
            ->update(['config_value'=>$request->post('config_value')]);
        return response()->json(['msg'=>'修改成功','status'=>'success']);
    }
    public function audit(AppConfig $appConfig)
    {
        return response()->json(new AppConfigResource($appConfig::query()->where('config_type',2)->get()),200);
    }


}
