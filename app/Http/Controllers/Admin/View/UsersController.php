<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/17
 * Time: 10:14
 */

namespace App\Http\Controllers\Admin\View;


use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index()
    {
        return view('admin.users.index');
    }
    public function register()
    {
        return view('admin.users.register');
    }
    public function team()
    {
        return view('admin.users.team');
    }


}
