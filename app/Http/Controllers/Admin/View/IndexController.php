<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/17
 * Time: 9:53
 */

namespace App\Http\Controllers\Admin\View;


class IndexController
{
    public function index()
    {
        return view('admin.index');
    }
}
