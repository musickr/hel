<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/17
 * Time: 10:14
 */

namespace App\Http\Controllers\Admin\View;


use App\AppConfig;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
    public function index()
    {
        return view('admin.config.money');
    }
    public function audit()
    {
        return view('admin.config.audit');
    }


}
