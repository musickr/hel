<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::query()
        ->where('menu_show','=',1)
        ->get();
        $tree  = $this->tree($menus,0);
        //dd($tree);
        return view('admin.home',['menus'=>$tree]);
    }
    public function tree($res,$id)
    {
        $tree = [];
        foreach ($res as $item => $re)
        {
            if($re->menu_pid == $id){
                $re->menu_child = $this->tree($res,$re->menu_id);
                $tree[] = $re;
            }
        }
        return $tree;
    }

}
