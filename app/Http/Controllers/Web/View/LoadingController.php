<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/14
 * Time: 11:18
 */

namespace App\Http\Controllers\Web\View;


use App\AppUser;
use App\Http\Controllers\Controller;

class LoadingController extends Controller
{
    static public function index($url='http://www.baidu.com')
    {
        return view('app.loading',['url'=>$url]);
    }

}
