<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:56
 */

namespace App\Http\Controllers\Web\View;


use App\Http\Controllers\Controller;
use App\Property;
use Illuminate\Support\Facades\Auth;

class GoldCoinDetailController extends Controller
{
    public function index(){
        $user = auth::guard('app')->user();
        $details = Property::query()
            -> where('property_uid',$user->user_id)
            -> get(['property_remark','created_at','property_num']);
        return view('app/goldCoinDetail',['details'=>$details]);
    }
}
