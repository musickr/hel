<?php

namespace App\Http\Controllers\Web\View;

use App\Massage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ServerController extends Controller
{
    //
    public function index()
    {
        $user = auth::guard('app')->user();
        return view('app.server',['user'=>$user]);
    }
    public function add(Request $request)
    {
        $data = $request->post();
        Massage::query()
            ->create($data);
    }
}
