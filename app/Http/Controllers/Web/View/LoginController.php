<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:56
 */

namespace App\Http\Controllers\Web\View;


use App\AppUser;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function index()
    {
        return view('app/login');
    }
    public function login(Request $request)
    {


        if(Auth::guard('app')->attempt([
            'user_account' => $request -> get('user_account'),
            'password'     => $request -> get('user_pwd')
        ])){
           return view('app.loading',['url'=>url('app/index/index'),'msg'=>'登陆成功']);
        }
        return view('app.loading',['url'=>url('app/index/index'),'msg'=>'账号密码错误']);
    }
    /**
      * 重写验证时使用的用户名字段
      */
    public function username()
    {
        return 'user_account';
    }
    public function guard()
    {
        return auth()->guard('app');
    }
    public function forget()
    {
        return view('app.forgetPwd');
    }
    public function forge(Request $request)
    {
        AppUser::query()->where('user_id',$request->post('user_id'))
            ->update(['user_pwd'=>bcrypt($request->post('user_pwd'))]);
        return view('app.loading',['url'=>url('app/index/index'),'msg'=>'修改成功']);
    }
}
