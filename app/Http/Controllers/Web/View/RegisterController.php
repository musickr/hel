<?php

namespace App\Http\Controllers\Web\View;

use App\AppUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    //
    public function index($id=0)
    {
        $referrer = auth::guard('app')->user();
        $node = AppUser::query()
            ->where('user_id',$id)
            ->first(['user_id','user_account']);
        return view('app/register',[
            'referrer'=>$referrer,
            'node'  => $node
        ]);
    }
    public function add(Request $request){
//    dd($request->post());

        $data = $request->post();
        unset($data['_token']);
        $user=AppUser::query()
            ->firstOrCreate($data);
        if($user)
        {
            return view('app.loading',['url'=>url('app/index/index')]);
        }

    }
}
