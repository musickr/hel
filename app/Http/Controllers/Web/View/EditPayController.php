<?php

namespace App\Http\Controllers\Web\View;

use App\UserAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EditPayController extends Controller
{
    //
    public function index()
    {
        return view('app/editPay');
    }
    public function edit(Request $request)
    {
        $data =[
            'ac_uid'=>$request->post('ac_uid'),
            'ac_bank_name'=>$request->post('ac_bank_name'),
            'ac_bank_num'=>$request->post('ac_bank_num'),
            'ac_zpay'=>$request->post('ac_zpay'),
            'ac_name'=>$request->post('ac_name'),
            'ac_wechat'=>$request->post('ac_wechat')
        ];

        $account = UserAccount::query()
            ->where('ac_id',$request->post('ac_id'))
            ->update($data);
        if($account)
        {
            return view('app.loading',['url'=>url('app/index/index'),'msg'=>'修改成功']);
        }
    }
}
