<?php

namespace App\Http\Controllers\Web\View;

use App\AppConfig;
use App\AppUser;
use App\Order;
use App\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuditController extends Controller
{
    //
    public function index()
    {
        return view('app/audit');
    }
    public function type(Request $request)
    {
        $user = auth::guard('app')->user();
        $type = $request->post('status');
        $list = Order::query()
            ->where(['order_to'=>$user->user_id,'order_status'=>$type])
            ->with('appUser:user_id,user_account,user_phone')
            ->get();

        return response()->json($list,200);
    }
    public function opr(Request $request)
    {
        $user = auth::guard('app')->user();


        $order=Order::query()
            ->find($request->post('order_id'));
        $use = AppUser::query()
            ->find($order->order_in);
        $use->user_role +=1;
            //->update(['order_status'=>$request->post('order_status')]);
        if($request->post('order_status')==1)
        {
            //需要的金币
            $propertyNum = AppConfig::query()
                ->where(['config_type'=>2,'config_name'=>$use->user_role])
                ->first()
                ->value('config_value');
            //拥有的金币
            $userProperty = Property::query()
                ->where('property_uid',$user->user_id)
                ->sum('property_num');
            //审核条件
            if($propertyNum <= $userProperty)
            {
                //通过审核
                $data = [
                    'property_uid'=>$user->user_id,
                    'property_in'=>$user->user_id,
                    'property_num'=>-$propertyNum,
                    'property_type'=>4,
                    'property_remark'=>'通过'.$use->user_account.'升级VIP'.$user->user_role.'的升级'
                ];
                Property::query()
                    ->create($data);
                $use->user_role_status=2;
                $use->save();
            }


        }
        $order->order_status = $request->post('order_status');
        $order->save();

    }
}
