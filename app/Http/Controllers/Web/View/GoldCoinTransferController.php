<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:56
 */

namespace App\Http\Controllers\Web\View;


use App\AppUser;
use App\Http\Controllers\Controller;
use App\Property;
use App\UserCoin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class GoldCoinTransferController extends Controller
{
    public function index(){
        $user = auth::guard('app')->user();
        $num = Property::query()
            ->where('property_uid',$user->user_id)
            ->sum('property_num');
        return view('app/goldCoinTransfer',['num'=>$num]);
    }
    public function transfer(Request $request)
    {
        $user = auth::guard('app')->user();
        $toUser = AppUser::query()->find($request->post('to_uid'));
        $num = Property::query()
            ->where('property_uid',$user->user_id)
            ->sum('property_num');
        if($request->post('property_num')>$num)
        {
            return '余额不足';
        }
        else
        {
            $data = [
                'property_uid'=>$request->post('to_uid'),
                'property_in'=>$user->user_id,
                'property_type'=>2,
                'property_remark'=>'收到'.$user->user_account.'的转账',
                'property_num'=>$request->post('property_num'),
            ];

            $res = Property::query()
                ->create($data);

            $data = [
                'property_uid'=>$user->user_id,
                'property_in'=>$request->post('to_uid'),
                'property_type'=>2,
                'property_remark'=>'转账给'.$toUser->user_account,
                'property_num'=>-$request->post('property_num'),
            ];
            $re = Property::query()
                ->create($data);
            if($res && $re){
                return view('app.loading',['url'=>url('app/index/index')]);
            }
        }

    }
}
