<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:56
 */

namespace App\Http\Controllers\Web\View;


use App\AppUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UpdateController extends Controller
{


    public function index(){
        $user = auth::guard('app')->user();

        $users = AppUser::all(['user_id','user_account','user_pid']);
        $tree = $this->pTree($users,$user->user_pid);

        return view('app/update',['parents'=>$tree,'userRole'=>$user->user_role]);
    }
    public function check()
    {

        $user  = auth::guard('app')->user();
        $role  = $user->user_role + 1;
        $users = AppUser::all();
        $tree  = $this->cTree($users,$user->user_id);
        $check = pow(2,$role)-2;
        $ret['res'] = count($tree) >= $check && $user->user_role_status == 1 ? true : false;
        return response()->json($ret,200);
    }

    public function pTree($res,$pid=0,$i=0)
    {
        static $pt = [];
        static $user_node = 0;
        $str = ['一','二','三','四','五','六','七','八'];
        if(empty($i))
        {
            $pt = [];
            $user_node = 0;
        }
        foreach ($res as $value)
        {
            if($value->user_id == $pid){
                $value->user_node = $str[$user_node];
                $user_node += 1;
                $i ++;
                $pt[] = $value;
                $this->pTree($res,$value->user_pid,$i);
            }
        }
        return $pt;
    }
    public function cTree($res,$id=0,$i=0)
    {
        static $pt = [];

        if(empty($i))
        {
            $pt = [];
        }
        foreach ($res as $value)
        {
            if($value->user_pid == $id){
                $i ++;
                $pt[] = $value;
                $this->cTree($res,$value->user_id,$i);
            }
        }
        return $pt;
    }
}
