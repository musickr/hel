<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:56
 */

namespace App\Http\Controllers\Web\View;


use App\Annunciate;
use App\AppUser;
use App\Http\Controllers\Controller;

class NotifyController extends Controller
{

    public function index($id){
        $notify = Annunciate::query()
            ->find($id);
        return view('app/notify',['notify'=>$notify]);
    }
    public function list()
    {
        $notifys = Annunciate::query()->orderBy('created_at','desc')->get();

        return view('app/notifyList',['notifys'=>$notifys]);
    }
}
