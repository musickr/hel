<?php

namespace App\Http\Controllers\Web\View;

use App\UserAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RegisterPayController extends Controller
{
    //
    public function index()
    {
        $user = auth::guard('app')->user();
        $account = UserAccount::query()
            ->where('ac_uid',$user->user_id)
            ->first();
        if($account){
            return view('app/editPay',['msg'=>$account]);
        }
        else{
            return view('app/registerPay',['user'=>$user]);
        }
    }
    public function add(Request $request)
    {
        $data = $request->post();
        unset($data['_token']);
        $account=UserAccount::query()
            ->create($data);
        if($account)
        {
            return view('app.loading',['url'=>url('app/index/index')]);
        }
    }

}
