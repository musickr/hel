<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:56
 */

namespace App\Http\Controllers\Web\View;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MeController extends Controller
{
    public function index(){
        $user = auth::guard('app')->user();
        return view('app/me',['user'=>$user]);
    }
    public function loginOut()
    {
        auth::guard('app')->logout();
        return redirect()->guest('app/index');
    }
}
