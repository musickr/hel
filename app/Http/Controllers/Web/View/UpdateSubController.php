<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:56
 */

namespace App\Http\Controllers\Web\View;



use App\AppConfig;
use App\AppUser;
use App\Http\Controllers\Controller;
use App\Order;
use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdateSubController extends Controller
{
    public function index()
    {
        $user  = auth::guard('app')->user();
        $users = AppUser::all();
        $userPid = $user->user_pid;
        $nextRole = $user->user_role+1;


        $nextRolePid = $this->check($users,$userPid,$nextRole);

        if($nextRolePid)
        {

            $conf = AppConfig::query()
                ->where('config_name','=',$nextRole)
                ->first(['config_value']);

            $account = AppUser::query()
                ->with('account:ac_uid,ac_name,ac_bank_name,ac_bank_num,ac_wechat,ac_zpay')
                ->get()
                ->find($nextRolePid)->toArray();

            return view('app/updateSub',[
                'money'=>$conf->config_value,
                'account'=>$account,
                'sub'=>[
                    'order_in'=>$user->user_id,
                    'order_to'=>$nextRolePid,
                ]
            ]);
        }
        return response()->json(['msg'=>'上级等级不足','status'=>'error']);

    }
    public function pTree($res,$pid=0,$i=0)
    {
        static $pt = [];
        static $user_node = 0;

        if(empty($i))
        {
            $pt = [];
            $user_node = 0;
        }
        foreach ($res as $value)
        {
            if($value->user_id == $pid){
                $user_node += 1;
                $value->user_node = $user_node;
                $i ++;
                $pt[] = $value;
                $this->pTree($res,$value->user_pid,$i);
            }
        }
        return $pt;
    }
    public function cTree($res,$id=0,$i=0)
    {
        static $pt = [];

        if(empty($i))
        {
            $pt = [];
        }
        foreach ($res as $value)
        {
            if($value->user_pid == $id){
                $i ++;
                $pt[] = $value;
                $this->cTree($res,$value->user_id,$i);
            }
        }
        return $pt;
    }
    protected function check($date,$userPid,$nextRole)
    {

        $parents = $this->pTree($date,$userPid);
        $nextRoleId = false;
        foreach ($parents as $parent)
        {
            if( $parent->user_node <= 8 && $parent->user_role >= $nextRole )
            {
                $nextRoleId = $parent->user_id;
                break;
            }
        }
        return $nextRoleId;
    }
    public function order(Request $request)
    {
        $user = auth::guard('app')->user();
        $user->user_role_status = 1;
        $user->save();
        $data = $request->post();


        /*$property = [
            'property_num' => $data['money'],
            'property_uid' => $data['order_to'],
            'property_in'  => $data['order_in'],
            'property_type'=> 1,
            'property_remark'=> $user->user_account.'升级到'.$user->user_role.'级收益'
        ];
        Property::query()
            ->create($property);*/
        unset($data['money']);
        $res  = Order::query()
            ->create($data);
        return response()->json($res,200);

    }

}
