<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:56
 */

namespace App\Http\Controllers\Web\View;


use App\AppUser;
use App\Http\Controllers\Controller;
use App\Property;
use Illuminate\Support\Facades\Auth;

class TeamController extends Controller
{
    public function index(){
        $user = auth::guard('app')->user();
        $users = AppUser::all();
        $referrer = AppUser::query()
            ->where('user_referrer',$user->user_id)
            ->count();
        $c = $this->cTree($users,$user->user_id);
        $lr = AppUser::query()
            ->where('user_pid',$user->user_id)
            ->get(['user_id','user_account','user_role']);
        foreach ($lr as $item => $value)
        {
            $value->child = count($this->cTree($users,$value->user_id));
            $value->num = Property::query()->where('property_uid',$value->user_id)->sum('property_num');
            $lr[$item] = $value;
        }

        return view('app/team',[
            'team'=>count($c),
            'user'=>$user,
            'referrer'=>$referrer,
            'child' => $lr
        ]);
    }
    public function cTree($res,$id=0,$i=0)
    {
        static $pt = [];

        if(empty($i))
        {
            $pt = [];
        }
        foreach ($res as $value)
        {
            if($value->user_pid == $id){
                $i ++;
                $pt[] = $value;
                $this->cTree($res,$value->user_id,$i);
            }
        }
        return $pt;
    }
    public function search($user_phone)
    {
        $user = AppUser::query()->where('user_phone',$user_phone)->first();
        $users = AppUser::all();
        if($user) {
            $referrer = AppUser::query()
                ->where('user_referrer', $user->user_id)
                ->count();
            $c = $this->cTree($users, $user->user_id);
            $lr = AppUser::query()
                ->where('user_pid', $user->user_id)
                ->get(['user_id', 'user_account', 'user_role']);
            foreach ($lr as $item => $value) {
                $value->child = count($this->cTree($users, $value->user_id));
                $value->num = Property::query()->where('property_uid', $value->user_id)->sum('property_num');
                $lr[$item] = $value;
            }

            return view('app/team', [
                'team' => count($c),
                'user' => $user,
                'referrer' => $referrer,
                'child' => $lr
            ]);
        }
        else
        {
            return view('app.loading',['url'=>url('app/team/index'),'msg'=>'用户未找到']);
        }
    }
}
