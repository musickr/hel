<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:56
 */

namespace App\Http\Controllers\Web\View;


use App\Annunciate;
use App\AppUser;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    use AuthenticatesUsers;
    public function index(){
        $user = AppUser::query()
                ->find(auth::guard('app')->user()->user_id);
        $notify = Annunciate::query()->orderBy('created_at','desc')->first(['notify_title','notify_id']);
        return view('app/index',['user'=>$user,'notify'=>$notify]);
    }
}
