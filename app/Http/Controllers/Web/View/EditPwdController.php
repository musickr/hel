<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:56
 */

namespace App\Http\Controllers\Web\View;


use App\AppUser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class EditPwdController extends Controller
{
    public function index(){
        return view('app/editPwd');
    }
    public function edit(Request $request)
    {

        $user = auth::guard('app')->user();
        $res = AppUser::query()
            ->where('user_id',$user->user_id)
            ->update(['user_pwd'=>bcrypt($request->post('user_pwd'))]);
        if($res)
        {
            return view('app.loading',['url'=>url('app/index/index'),'msg'=>'修改成功']);
        }
    }

}
