<?php

namespace App\Http\Resources;

use App\AppUser;
use App\Property;
use Illuminate\Http\Resources\Json\ResourceCollection;;

class AppUserResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $users = AppUser::all();
        $data = $this->collection;
        foreach ($data as $item => $value)
        {
            $value['user_ref'] = AppUser::query()
                ->where('user_id',$value['user_referrer'])
                ->value('user_account');
            $value['user_node'] = AppUser::query()
                ->where('user_id',$value['user_pid'])
                ->value('user_account');
            $value['user_coin'] = Property::query()
                ->where('property_uid',$value['user_id'])
                ->sum('property_num');
            $data[$item] = $value;
        }

        return [
            'code' => 0,
            'msg'  => "用户信息列表",
            'count' => $users->count(),
            'data' => $data,
        ];

    }
}
