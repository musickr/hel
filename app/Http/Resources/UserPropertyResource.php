<?php

namespace App\Http\Resources;


use App\AppUser;
use App\Property;
use Illuminate\Http\Resources\Json\ResourceCollection;;

class UserPropertyResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $type = ['未知',1=>'升级转账收入',2=>'升级转账支出',3=>'系统充值'];
        $propertys = Property::all();
        $data = $this->collection;
        foreach ($data as $item => $value)
        {
            $value['property_user'] = AppUser::query()
                ->where('user_id',$value['property_uid'])
                ->value('user_account');
            $value['property_in'] = AppUser::query()
                ->where('user_id',$value['property_in'])
                ->value('user_account');
            $value['property_type'] = $type[$value['property_type']];
            $data[$item] = $value;
        }

        return [
            'code' => 0,
            'msg'  => "用户资金列表",
            'count' => $propertys->count(),
            'data' => $data,
        ];

    }
}
