<?php

namespace App\Http\Resources;


use App\AppUser;
use App\Property;
use Illuminate\Http\Resources\Json\ResourceCollection;;

class AppConfigResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $type = [
            '未知',
            '升级VIP1',
            '升级VIP2',
            '升级VIP3',
            '升级VIP4',
            '升级VIP5',
            '升级VIP6',
            '升级VIP7',
            '升级VIP8',
        ];

        foreach($this->collection as $item => $value)
        {
            $value->config_name = $type[$value->config_name];
            $this->collection[$item] = $value;
        }
        return [
            'code' => 0,
            'msg'  => "",
            'count' => 0,
            'data' => $this->collection,
        ];

    }
}
