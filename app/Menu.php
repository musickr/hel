<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    protected $table = 'app_menu';
    protected $primaryKey = 'menu_id';
}
