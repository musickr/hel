<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    //
    protected $table = 'app_user_account';
    protected $primaryKey = 'ac_id';
    protected $fillable = [
        'ac_uid',
        'ac_bank_name',
        'ac_bank_num',
        'ac_zpay',
        'ac_name',
        'ac_wechat'
    ];

}
