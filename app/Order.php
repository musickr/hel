<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'app_order';
    protected $primaryKey = 'order_id';
    protected $fillable = [
        'order_in',
        'order_to',
        'order_type',
        'order_status'
    ];
    public function appUser()
    {
        return $this->belongsTo('App\AppUser','order_in','user_id');
    }
    public function getCreatedAtAttribute()
    {
        return date('Y/m/d H:i', strtotime($this->attributes['created_at']));
    }

}
