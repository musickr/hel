<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    //
    protected $table = 'app_user_property';
    protected $fillable = [
        'property_uid',
        'property_num',
        'property_type',
        'property_remark',
        'property_in'
    ];
}
