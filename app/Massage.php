<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Massage extends Model
{
    //
    protected $table = 'app_msg';
    protected $primaryKey = 'msg_id';
    protected $fillable = [
        'msg_uid',
        'msg_type',
        'msg_content'
    ];
}
