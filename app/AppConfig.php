<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppConfig extends Model
{
    //
    protected $table = 'app_config';
    protected $fillable = [
        'config_name',
        'config_value',
        'config_type',
    ];

}
